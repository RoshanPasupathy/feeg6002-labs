#include <stdio.h>
#include<limits.h>
#include <math.h>
#define N 12

long maxlong(void);
double upper_bound(long n);
long factorial(long n);

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
	printf("factorial of N  is %ld\n", factorial(N));
    return 0;
}

long maxlong(void){
	return LONG_MAX;
}

double upper_bound(long n){
	if (n >= 0  && n < 6){
		return 719;
	}
	if (n >=6){
		return pow((double) n/2, (double) n);
	}
	else {
		printf("UNDEFINED BEHAVIOUR\n");
		return -1;
	}
}

long factorial(long n){
	if (upper_bound(n) < maxlong()){
		if (n == 1 || n == 0){
			return 1;
		}
		else if (n > 1){
			return n * factorial(n - 1);
		}
		else {
			return -2;
		}
	}
	else {
		return -1;
	}
}
