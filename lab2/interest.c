#include<stdio.h>

int main(){
	int s = 1000;
	double debt = s;
	double rate = 0.03;
	double interest = 0;
	double cinterest =0;
	int month;
	for (month = 1; month < 25; month++){
		interest = debt*rate;
		cinterest += interest;
		debt *= 1 + rate;
		printf("month %2d: debt=%7.2f, interest = %7.2f,\
total_interest = %7.2f, frac =%2.2f%%\n",month,\
debt, interest, cinterest, cinterest*100/s);
	}
	return 0;
}
