/* Laboratory 6, FEEG6002, 2016/2017, Template */

#include <stdio.h>
#include<string.h>

/* Function void rstrip(char s[])
modifies the string s: if at the end of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/

void rstrip(char s[]) {
    /* to be implemented */
	long wspaces = 0;
	long i;
	int len_s = strlen(s);
	while (s[len_s - 1 - wspaces] == ' '){
		wspaces++;
	}
	for (i = 0; i <= len_s - 1 - wspaces; i++){
		s[i] = s[i];
	}
	/*printf("value of len_s: %d\n", len_s);*/
	s[len_s - wspaces] = '\0';	
}


int main(void) {
  char test1[] = "Hello World   ";

  printf("Original string reads  : |%s|\n", test1);
  rstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);

  return 0;
}
