import cython
#cimport numpy as np
import numpy as np
#from cpython.array cimport clone,array

#from libc.stdlib cimport calloc,free
#from cpython.array cimport array, clone

#cdef extern from "array_alloc.h":
#    long** alloc_2d_long(long ndim1, long ndim2);


cdef extern from "complex.h":
    double cabs(double complex z)
    double creal(double complex z)
    double cimag(double complex z)

# cdef extern from "complexobject.h":

#     struct Py_complex:
#         double real
#         double imag

#     ctypedef class __builtin__.complex [object PyComplexObject]:
#         cdef Py_complex cval


# cdef extern from "math.h":
#     double sqrt(double x)

#cdef double abs_cy(complex c):
#    return ((c.cval.real**2 + c.cval.imag**2))**(0.5)


@cython.boundscheck(False)
@cython.cdivision(True)
@cython.wraparound(False)
def mandel_cy(long n,int itermax=100,double xmin=-2,double xmax=0.5,double ymin=-1.25,double ymax=1.25):
    '''
    Mandelbrot fractal computation using Python.

    (n, n) are the output image dimensions
    itermax is the maximum number of iterations to do.
    xmin, xmax, ymin, ymax specify the region of the
    set to compute.

    Returns a list of lists of ints, representing the image matrix with
    n rows and n columns.
    '''
    cdef:
        double x = xmin
        double y = ymin
        #complex z
        double xdiff, ydiff
        #complex double c,z
        #long it #may not be needed
        long ix,iy
        int xy = 0

        double complex z

    # cdef double complex[:] zarr = np.zeros((n*n),np.complex128)
    # cdef double complex* z = &zarr[0]

        #long* itsp = alloc_2d_long(n, n)

    #cdef array its =  np.zeros((n,n), dtype=np.int64) #cpython array.array fastest
    #                                                    but wont work

    cdef int[:,:] its = np.zeros((n,n), dtype=np.int32)
    cdef int* itsp = &its[0,0]

    xdiff = (xmax - xmin) / float(n)
    ydiff = (ymax - ymin) / float(n)
    # create list containing n lists, each containing n zeros
    # (i.e. a matrix, represented as a list of lists)

    #its = [ [0] * n for i in range(n)]

    # The data in the matrix are iterations, so 'its' is the plural of
    # IT for ITeration.


    # iterate through all matrix elements
    for ix in xrange(0, n):
        y = ymin
        #x = xmin + ix * xdiff
        for iy in xrange(0, n):
            # compute the position (x, y) corresponding to matrix element
            #y = ymin + iy * ydiff
            # Need to count iterations

            # c is the complex number with the given
            # x, y coordinates in the complex plane, i.e. c = x + i * y
            # where i = sqrt(-1)
            #c = x + y * 1j

            z = 0 + 0 * 1j

            #za[0] = 0.0
            #za[1] = 0.0

            # Here is the actual Mandelbrot criterion: we update z to be
            # z <- z^2 + c until |z| <= 2. We could the number of iterations
            # required. This number of iterations is the data we need to compute
            # (and plot if desired).
            #while it < itermax and abs_cy(z) < 2.0:
            while itsp[xy] < itermax and cabs(z) < 2.0:
            #while itsp[(ix*n) + iy] < itermax and cabs(z) < 2.0:
            #while it < itermax and (za[0]**2 + za[1]**2)**0.5  < 2.0:
                #z = z.cval.real**2 - z.cval.imag**2 + x + (2*z.cval.real*z.cval.imag + y)*1j
                z = creal(z)**2 - cimag(z)**2 + x + (2*creal(z)*cimag(z) + y)*1j
                #itsp[(ix*n) + iy] = it
                itsp[xy] += 1
            # if ((ix == 1) * (iy == 149)) + ((ix == 1) * (iy == 150)):
            #     print("x = %f, y = %f at (1,%d)" %(x,y, iy))
            #     print("zval = ")
            #     print(z)
            # if (ix == 1) * (iy == 150):
            #     print("x = %f, y = %f at (1,150)" %(x,y))
            xy += 1
            y += ydiff
        x += xdiff

            #print("ix={}, iy={}, x={}, y={}, c={}, z={}, abs(z)={}, it={}"
            #    .format(ix, iy, x, y, c, z, abs(z), it))

            # Store the result in the matrix
            #its[ix][iy] = it

            #itsp[ix][iy] = it
    #print("***************this is for working function***********")
    return its

@cython.boundscheck(False)
@cython.cdivision(True)
@cython.wraparound(False)
def mandel_cy_z(long n,int itermax=100,double xmin=-2,double xmax=0.5,double ymin=-1.25,double ymax=1.25):
    '''
    Mandelbrot fractal computation using Python.

    (n, n) are the output image dimensions
    itermax is the maximum number of iterations to do.
    xmin, xmax, ymin, ymax specify the region of the
    set to compute.

    Returns a list of lists of ints, representing the image matrix with
    n rows and n columns.
    '''
    cdef:
        double x, y
        #complex z
        double xdiff, ydiff
        #complex double c,z
        #long it #may not be needed
        long ix,iy
        int xy = 0

        #double complex z

    cdef double complex[:] zarr = np.zeros((n*n),np.complex128)
    cdef double complex* z = &zarr[0]

        #long* itsp = alloc_2d_long(n, n)

    #cdef array its =  np.zeros((n,n), dtype=np.int64) #cpython array.array fastest
    #                                                    but wont work

    cdef unsigned int[:,:] its = np.zeros((n,n), dtype=np.uint32)
    cdef unsigned int* itsp = &its[0,0]

    xdiff = (xmax - xmin) / float(n)
    ydiff = (ymax - ymin) / float(n)
    # create list containing n lists, each containing n zeros
    # (i.e. a matrix, represented as a list of lists)

    #its = [ [0] * n for i in range(n)]

    # The data in the matrix are iterations, so 'its' is the plural of
    # IT for ITeration.


    # iterate through all matrix elements
    for ix in xrange(0, n):
        x = xmin + ix * xdiff
        for iy in xrange(0, n):
            # compute the position (x, y) corresponding to matrix element
            y = ymin + iy * ydiff
            # Need to count iterations

            # c is the complex number with the given
            # x, y coordinates in the complex plane, i.e. c = x + i * y
            # where i = sqrt(-1)
            #c = x + y * 1j

            #z = 0 + 0 * 1j

            #za[0] = 0.0
            #za[1] = 0.0

            # Here is the actual Mandelbrot criterion: we update z to be
            # z <- z^2 + c until |z| <= 2. We could the number of iterations
            # required. This number of iterations is the data we need to compute
            # (and plot if desired).
            #while it < itermax and abs_cy(z) < 2.0:
            while itsp[xy] < itermax and cabs(z[xy]) < 2.0:
            #while itsp[(ix*n) + iy] < itermax and cabs(z) < 2.0:
            #while it < itermax and (za[0]**2 + za[1]**2)**0.5  < 2.0:
                #z = z.cval.real**2 - z.cval.imag**2 + x + (2*z.cval.real*z.cval.imag + y)*1j
                z[xy] = creal(z[xy])**2 - cimag(z[xy])**2 + x + (2*creal(z[xy])*cimag(z[xy]) + y)*1j
                #itsp[(ix*n) + iy] = it
                itsp[xy] += 1
            # if ((ix == 1) * (iy == 149)) + ((ix == 1) * (iy == 150)):
            #     print("x = %f, y = %f at (1,%d)" %(x,y, iy))
            #     #print("ydiff = %f and y + ydiff = %f" %(ydiff, y + ydiff))
            #     print('zval = ')
            #     print(z[xy])
            xy += 1

            #print("ix={}, iy={}, x={}, y={}, c={}, z={}, abs(z)={}, it={}"
            #    .format(ix, iy, x, y, c, z, abs(z), it))

            # Store the result in the matrix
            #its[ix][iy] = it

            #itsp[ix][iy] = it

    return its