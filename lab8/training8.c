#include<stdio.h>
#include<string.h>
#include<stdlib.h>

char* mix(char * s1, char* s2){
	char* r = (char *) malloc(sizeof(char *) * (2 * strlen(s1) + 1));
	int i;
	if (r == NULL){
		return NULL;
	}
	else {
		for (i = 0; i < strlen(s1); i++){
			r[2*i] = s1[i];
			r[2*i + 1] = s2[i];
		}
		r[2 * strlen(s1)] = '\0';
		return r;
	}
}

void use_mix(void) {
    char s1[] = "Hello World";
    char s2[] = "1234567890!";

    printf("s1 = %s\n", s1);
    printf("s2 = %s\n", s2);
    printf("r  = %s\n", mix(s1, s2));
}

int main(){
	use_mix();
	return 0;
}

