#include <stdio.h>

int main(){
	int c;
	fprintf(stderr, "press Ctrl-D to quit\n");
	while ((c = getchar())!= EOF)
	{
		putchar(c);
	}
	fprintf(stderr, "Goodbye\n");
	return 0;
}
