#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

/* function prototype */
void reverse(char source[], char target[]);
long string_length(char s[]);

int main(void) {
  char original[] = "This is a test: can you print me in reverse character order?";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  return 0;
}

/* reverse the order of characters in 'source', write to 'target'.
   Assume 'target' is big enough. */
void reverse(char source[], char target[]) {
	long len_source = string_length(source);
	long i;
	for (i = 0; i < len_source; i++){
		target[i] = source[len_source - i - 1];
	}
	target[len_source] = '\0';
}

long string_length(char s[]){
        long count =0;
        while (s[count] != '\0'){
                count++;
        }
        return count;
}

